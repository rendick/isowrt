# isowrt

Easy way to burn a bootable flash drive with ISO.

[Bash Shell version](https://github.com/rendick/isowrt)

# Available OS

**All UNIX systems are supported.**

# Install

```bash
git clone https://gitlab.com/rendick/isowrt.git
cd isowrt
./main
```

**Soon I will make a PKGBUILD file for Arch-based distributions.**

# Compile 

```bash
git clone https://gitlab.com/rendick/isowrt.git
cd isowrt
rustc src/main.rs
./main
```

# License

**GNU AGPL v3**

